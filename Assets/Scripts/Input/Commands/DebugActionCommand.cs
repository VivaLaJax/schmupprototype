﻿using System;

public class DebugActionCommand : Command
{
    public override void execute(Actor actor)
    {
        actor.onDebugInput();
    }
}