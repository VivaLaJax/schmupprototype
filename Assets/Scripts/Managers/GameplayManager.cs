﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayManager : MonoBehaviour
{
    float gameStartTime;
    float lastLevelIncreaseTime;
    public List<GameObject> allPlayers;
    int enemyCount;
    int pointsTotal;
    public int gameLevel;

    public GameObject gameoverScreen;

    const float LEVEL_INCREASE_PERIOD = 20;
    void Awake()
    {
        resetGameTimers();
        allPlayers = new List<GameObject>(GameObject.FindGameObjectsWithTag("Player"));
        enemyCount = 0;
        gameLevel = 1;
    }

    public void resetGame()
    {
        gameLevel = 1;
        enemyCount = 0;
        resetGameTimers();
        resetPlayers();
        gameoverScreen.SetActive(false);
        enableAllSpawners();
    }

    public void resetPlayers()
    {
        foreach (GameObject player in allPlayers)
        {
            player.SetActive(true);
            PlayerCharacter playerCharacter = player.GetComponent<PlayerCharacter>();
            playerCharacter.reset();
        }
    }

    public void resetGameTimers()
    {
        gameStartTime = Time.time;
        lastLevelIncreaseTime = gameStartTime;
    }

    public float getCurrentGameTime()
    {
        return Time.time - gameStartTime;
    }

    public List<GameObject> getAllPlayers()
    {
        return allPlayers;
    }

    public List<GameObject> getAllActivePlayers()
    {
        List<GameObject> activePlayers = new List<GameObject>();
        foreach (GameObject player in allPlayers)
        {
            PlayerCharacter playerCharacter = player.GetComponent<PlayerCharacter>();
            if (playerCharacter.isAlive())
            {
                activePlayers.Add(player);
            }
        }
        return activePlayers;
    }

    public int getEnemyCount()
    {
        return enemyCount;
    }

    public int getPoints()
    {
        return pointsTotal;
    }

    public void addPoints(int pointsToAdd)
    {
        pointsTotal += pointsToAdd;
    }

    public int getLevel()
    {
        return gameLevel;
    }

    public void onEnemySpawned(GameObject enemy)
    {
        enemyCount++;
    }

    public void onEnemyDestroyed(GameObject enemy, int pointsValue)
    {
        addPoints(pointsValue);
        enemyCount--;
    }

    public void onPlayerSpawned(GameObject player)
    {
        allPlayers.Add(player);
    }

    public void onPlayerDied(GameObject player)
    {
        if (checkAllPlayersDead())
        {
            initiateGameOver();
        }
    }

    public bool checkAllPlayersDead()
    {
        foreach (GameObject player in allPlayers)
        {
            PlayerCharacter playerCharacter = player.GetComponent<PlayerCharacter>();
            if (playerCharacter.isAlive())
            {
                return false;
            }
        }

        return true;
    }

	private void initiateGameOver()
	{
		disableAllSpawners ();
		destroyAllEnemies ();
		gameoverScreen.SetActive (true);
        gameoverScreen.GetComponent<GameoverScreen>().open();
	}

	public void disableAllSpawners()
	{
		setEnabledOnAllSpawnersAndReset (false);
	}

	public void enableAllSpawners()
	{
		setEnabledOnAllSpawnersAndReset (true,true);
	}

	private void setEnabledOnAllSpawnersAndReset(bool enabled, bool reset=false)
	{
		GameObject[] spawners = GameObject.FindGameObjectsWithTag ("Spawn");
		foreach(GameObject spawnerObject in spawners)
		{
			Spawner spawner = spawnerObject.GetComponent<Spawner> ();
			spawner.enabled = enabled;
			if (reset)
			{
				spawner.reset ();
			}
		}
	}

	private void destroyAllEnemies()
	{
		GameObject[] enemies = GameObject.FindGameObjectsWithTag ("Enemy");
		foreach(GameObject enemy in enemies)
		{
			Destroy (enemy);
		}
	}

    void Update()
    {
        checkGameLevel();
    }

    private void checkGameLevel()
    {
        if(getCurrentGameTime()-(lastLevelIncreaseTime+LEVEL_INCREASE_PERIOD)>=0)
        {
            gameLevel++;
            lastLevelIncreaseTime = getCurrentGameTime();
        }
    }
}
