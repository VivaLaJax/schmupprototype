﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsTotalText : MonoBehaviour
{
    private GameplayManager manager;
    private Text pointsText;
    private int previousPointsTotal;

	void Start ()
    {
        manager = GameObject.Find("GameplayManager").GetComponent<GameplayManager>();
        pointsText = gameObject.GetComponent<Text>();
    }
	
	void Update ()
    {
        if(manager==null)
        {
            return;
        }

        int points = manager.getPoints();
		if(points!=previousPointsTotal)
        {
            updatePointsText(points);
            previousPointsTotal = points;
        }
	}

    private void updatePointsText(int points)
    {
        pointsText.text = points.ToString();
    }
}
