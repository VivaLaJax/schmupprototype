﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * The aim of the input manager is to abstract away the necessity for having to
 * put input polling into every class that needs to respond to user input and then
 * orchestrate between them.
 * 
 * Concerns:
 * - Since this is a one player game, we only need to potentially have one input focus. Some
 *  action games provide movement focus whilst allowing other buttons to control UIs at the same time.
 *  We may need this for camera control whilst also affecting UIs.
 *  - We may want to be able to map different user input to different actions to make it easier to play
 *  
 *  With the above in mind, the structure is that the objects which require input, inform the input manager
 *  that they are taking focus (and relinquishing focus). The default will always be the player. The input
 *  manager will store commands and associate them to button mappings. A command can be called on the focus
 *  and the focus can then handle that input however it likes.
 *  
 *  19/02 23:00 - Unity actually has an input manager for mapping the buttons. But it doesn't handle the assigment
 *  of in game commands (obviously). So this is still useful. Essentially the model becomes
 *  
 *  User Input -> Input Name -> Command -> In Game Implementation based on Focus
 *  
 *  Needs to handle:
 *  - 4-Directional Movement
 *  - Initiate Interaction
 *  - UI Confirm
 *  - UI Cancel
 *  - UI Directions
 *  
 *  Reminder:
 *  - Add a debug button
 *  - Add logging/debug options (Log Button Press, Button history?, Print Mappings, Print Focus, Focus History?)
 */
public class InputManager : MonoBehaviour
{
    	/*
     	* Take in input
     	* If mapped command
     	* call mapped command on UI focus
    	*/

	readonly string HORIZONTAL_AXIS_INPUT_STRING = "Horizontal";
	readonly string VERTICAL_AXIS_INPUT_STRING = "Vertical";
	readonly string ACTION_CONFIRM_INPUT_STRING = "Confirm";
	readonly string ACTION_CANCEL_INPUT_STRING = "Cancel";
	readonly string ACTION_MENU_INPUT_STRING = "Menu";
	readonly string SECRET_DEBUG_ACTION_STRING = "Debug";
	readonly string SHOOT_HORIZONTAL_AXIS_INPUT_STRING = "ShootHorizontal";
	readonly string SHOOT_VERTICAL_AXIS_INPUT_STRING = "ShootVertical";

	readonly int COMMAND_STREAM_EVENT_MAX = 20;

	//I don't really like this impl but it's quick
	public string playerInputExtension;
    

	public GameObject defaultActorSource;
	Stack<Actor> currentInputFocus;
	Queue<Command> commandStream;

	void Start ()
	{
	        commandStream = new Queue<Command>();
        	currentInputFocus = new Stack<Actor>();

        	Actor defaultActor = defaultActorSource.GetComponent<Actor>();
	        currentInputFocus.Push(defaultActor);
    	}
	
	void Update ()
	{
        	List<Command> currCommands = handleInput();
		foreach(Command command in currCommands)
		{
			if (command != null)
			{
				addToCommandStream(command);
				command.execute(currentInputFocus.Peek());
			}
		}
	}

	private void addToCommandStream(Command currCommand)
	{
		if(commandStream.Count==COMMAND_STREAM_EVENT_MAX)
		{
			commandStream.Dequeue();
		}
		commandStream.Enqueue(currCommand);
	}

	private List<Command> handleInput()
	{
		List<Command> commandsThisFrame = new List<Command>();

		readPrimaryAxes (ref commandsThisFrame);
		readSecondaryAxes (ref commandsThisFrame);
		readButtons (ref commandsThisFrame);

		return commandsThisFrame;
	}

	private void readPrimaryAxes (ref List<Command> commandsThisFrame)
	{
		if (Input.GetAxisRaw(HORIZONTAL_AXIS_INPUT_STRING+playerInputExtension)!=0 ||
            Input.GetAxisRaw(VERTICAL_AXIS_INPUT_STRING + playerInputExtension) != 0)
		{
			commandsThisFrame.Add(new HorizontalVerticalActionCommand(Input.GetAxisRaw(HORIZONTAL_AXIS_INPUT_STRING+playerInputExtension), 
                                                                Input.GetAxisRaw(VERTICAL_AXIS_INPUT_STRING + playerInputExtension)));
		}
		
	}

	private void readSecondaryAxes (ref List<Command> commandsThisFrame)
	{
		if (Input.GetAxisRaw(SHOOT_HORIZONTAL_AXIS_INPUT_STRING+playerInputExtension)!=0 ||
            Input.GetAxisRaw(SHOOT_VERTICAL_AXIS_INPUT_STRING + playerInputExtension) != 0)
		{
			commandsThisFrame.Add(new HorizontalVerticalActionCommand(Input.GetAxisRaw(SHOOT_HORIZONTAL_AXIS_INPUT_STRING+playerInputExtension),
                                                                Input.GetAxisRaw(SHOOT_VERTICAL_AXIS_INPUT_STRING + playerInputExtension), HorizontalVerticalActionCommand.SECONDARY_AXIS));
		}
	}

	private void readButtons(ref List<Command> commandsThisFrame)
	{
		//Want to GetButton because we can handle holding down the fire input
		if (Input.GetButton(ACTION_CONFIRM_INPUT_STRING+playerInputExtension))
		{
			commandsThisFrame.Add( new ConfirmActionCommand());
		}
		else if (Input.GetButtonDown(ACTION_CANCEL_INPUT_STRING+playerInputExtension))
		{
			commandsThisFrame.Add( new CancelActionCommand());
		}
		else if (Input.GetButtonDown(ACTION_MENU_INPUT_STRING+playerInputExtension))
		{
			commandsThisFrame.Add( new MenuActionCommand());
		}
		else if (Input.GetButtonDown(SECRET_DEBUG_ACTION_STRING+playerInputExtension))
		{
			commandsThisFrame.Add( new DebugActionCommand());
		}
	}

	public void takeInputFocus(Actor actor)
	{
		currentInputFocus.Push(actor);
	}

	public void relinquishInputFocus(Actor actor)
	{
		if(currentInputFocus.Peek().Equals(actor))
		{
			currentInputFocus.Pop();
		}

		if(currentInputFocus.Count==0)
		{
			Actor defaultActor = defaultActorSource.GetComponent<Actor>();
			currentInputFocus.Push(defaultActor);
		}
	}
}
