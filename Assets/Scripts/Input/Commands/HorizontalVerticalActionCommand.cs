﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalVerticalActionCommand : Command 
{
    public const int PRIMARY_AXIS = 0;
    public const int SECONDARY_AXIS = 1;

    private float horizontalAxisInput;
    private float verticalAxisInput;
    private int axis;

    public HorizontalVerticalActionCommand(float horizontalAxis, float verticalAxis, int axis = 0)
    {
        horizontalAxisInput = horizontalAxis;
        verticalAxisInput = verticalAxis;
        this.axis = axis;
    }

    void Start () 
	{

	}

	public float getHorizontalAxis()
	{
		return horizontalAxisInput;
	}

	public float getVerticalAxis()
	{
		return verticalAxisInput;
	}

	public override void execute (Actor actor)
	{
		if (axis == PRIMARY_AXIS)
		{
			actor.onFullPrimaryAxisInput(horizontalAxisInput, verticalAxisInput);
		} 
		else if (axis == SECONDARY_AXIS)
		{
			actor.onFullSecondaryAxisInput(horizontalAxisInput, verticalAxisInput);
		}
	}
}
