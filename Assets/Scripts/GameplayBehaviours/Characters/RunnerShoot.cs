﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunnerShoot : MonoBehaviour, IShoot 
{
	public GameObject bullet;

	public int range;
	public int damage;

	public float fireRate;
	private float lastFireTime;

	//Maybe I can pull this into a character class
	PlayerCharacter playerCharacter;
    public AudioClip shootSound;

	void Start () 
	{
		lastFireTime = Time.time - fireRate;
		playerCharacter = gameObject.GetComponent<PlayerCharacter> ();
		if (playerCharacter == null) 
		{
			Debug.LogError ("Oh no! Can't find the Player Character script");
		}
	}

	void Update () 
	{
		
	}

	public void fire (Vector3 fireDirection)
	{
        if (!gameObject.activeSelf)
        {
            return;
        }

        if (timeToFire ())
		{
			GameObject bulletInstance = Instantiate (bullet, gameObject.transform.position+fireDirection, gameObject.transform.rotation);
			bulletInstance.transform.rotation = Quaternion.LookRotation(fireDirection);

			Bullet bulletScript = bulletInstance.GetComponent<Bullet> ();
			bulletScript.setRange (range);
			bulletScript.setDamage (damage);
			bulletScript.setTargetTag ("Enemy");
			bulletScript.setSpeed (playerCharacter.getSpeed());
			bulletScript.setSource (gameObject);
            
			lastFireTime = Time.time;

            playerCharacter.playSoundEffect(shootSound);
		}
	}

	private bool timeToFire()
	{
		if (fireRate == 0)
		{
			return false;
		}

		float firePeriod = (float)(1 / fireRate);
		float timeSinceLastFire = Time.time - lastFireTime;
		if (timeSinceLastFire >= firePeriod)
		{
			return true;
		}

		return false;
	}
}
