﻿using System;

public class CancelActionCommand : Command
{
    public override void execute(Actor actor)
    {
        actor.onCancel();
    }
}