﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Just a quick knock up for handling the input commands that get thrown down from the InputHandler
 * 
 */
public class PlayerHandle : MonoBehaviour, Actor
{
    PlayerMovement playerMovementScript;
	IShoot shootInterface;

    public void Start()
    {
        playerMovementScript = gameObject.GetComponent<PlayerMovement>();
        if(playerMovementScript==null)
        {
            Debug.LogError("Oh no! Couldn't find playerMovementScript");
        }
		shootInterface = gameObject.GetComponent<IShoot> ();
		if (shootInterface == null) 
		{
			Debug.LogError("Oh no! Couldn't find any shooter interface");
		}
    }
		
    public void onCancel()
    {
        Debug.Log("Cancel Button Pressed");
    }

    public void onConfirm()
    {
        Debug.Log("Confirm Button Pressed");
    }

    public void onDebugInput()
    {
        //throw new NotImplementedException();
    }

    public void onHorizontalInput(float horizontalAxisInput)
    {
        playerMovementScript.onHorizontalInput(horizontalAxisInput);
    }

    public void onMenu()
    {
        //throw new NotImplementedException();
    }

    public void onVerticalInput(float verticalAxisInput)
    {
        playerMovementScript.onVerticalInput(verticalAxisInput);
    }

	public void onSecondaryAxisHorizontalInput (float horizontalAxisInput)
	{
		if (horizontalAxisInput > 0)
		{
			shootInterface.fire (transform.right);	
		} 
		else if (horizontalAxisInput < 0)
		{
			shootInterface.fire (transform.right * -1);
		}
	}

	public void onSecondaryAxisVerticalInput (float verticalAxisAmount)
	{
		if (verticalAxisAmount > 0)
		{
			shootInterface.fire (transform.forward);	
		} 
		else if (verticalAxisAmount < 0)
		{
			shootInterface.fire (transform.forward * -1);
		}
	}

	public void onFullPrimaryAxisInput (float horizontalAxisInput, float verticalAxisInput)
	{
		playerMovementScript.onFullAxisInput(horizontalAxisInput,verticalAxisInput);
	}

	public void onFullSecondaryAxisInput (float horizontalAxisInput, float verticalAxisInput)
	{
		Vector3 direction = Vector3.zero;
		if (horizontalAxisInput > 0)
		{
			direction = transform.right;
		} 
		else if (horizontalAxisInput < 0)
		{
			direction = (transform.right * -1);
		}
		if (verticalAxisInput > 0)
		{
			direction += transform.forward;
		} 
		else if (verticalAxisInput < 0)
		{
			direction+= (transform.forward * -1);
		}
		shootInterface.fire (direction);
	}
}
