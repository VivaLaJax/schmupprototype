﻿using UnityEngine;

/*
 * The character class is set up as a base class for NPCs and the player character but it could be
 * an interface if that's more useful? It's a way to identify to the interaction system
 * any game objects which can initiate an interaction.
 */

public class Character : MonoBehaviour
{
    public Character()
    {

    }
}