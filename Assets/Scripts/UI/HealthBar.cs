﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour 
{
	public GameObject playerToWatch;
	PlayerCharacter playerCharacter;
	public float maxWidth;

	RectTransform rectTransform; 

	void Start () 
	{
		rectTransform = gameObject.GetComponent<RectTransform> ();
		maxWidth = rectTransform.rect.width;
		if(playerToWatch==null) 
		{
			Debug.LogError ("Uh oh! Player to watch is NULL. This isn't gonna work!");
		}
		playerCharacter = playerToWatch.GetComponent<PlayerCharacter> ();
		if(playerCharacter==null) 
		{
			Debug.LogError ("Uh oh! PlayerCharacter script is NULL. This isn't gonna work!");
		}
	}
	
	void Update () 
	{
			rectTransform.sizeDelta = new Vector2 ((maxWidth * playerCharacter.getHealthPercentageDecimal ()),
				rectTransform.rect.height);
	}
}
