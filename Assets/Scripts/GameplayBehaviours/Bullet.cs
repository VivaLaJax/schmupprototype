﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour 
{
	float damage = 1.0f;
	float range = 40.0f;
	float distanceTravelled = 0f;
	float speed = 2.0f;

	string targetTag = "";
	GameObject sourceObject = null;

	void Start () 
	{
		
	}

	void Update () 
	{
		float distanceToTravel = Time.deltaTime * speed;
		transform.position += transform.forward * distanceToTravel;
		distanceTravelled += distanceToTravel;
		if (distanceTravelled >= range) 
		{
			Destroy (gameObject);
		}
	}

	void OnCollisionEnter(Collision collision)
	{
		if (shouldIgnoreCollision(collision)) 
		{
			return;
		}

		if(collision.gameObject.CompareTag(targetTag))
		{
			Debug.Log ("Collided with the target");
			Damageable damageableInterface = collision.gameObject.GetComponent<Damageable> ();
			if (damageableInterface!=null) 
			{
				Debug.Log ("Dealing damage to the target");
				damageableInterface.onDamageReceived (damage);
			}
		}
		Debug.Log ("Collieded with " + collision.gameObject.name);
		Destroy (gameObject);
	}

	private bool shouldIgnoreCollision(Collision collision)
	{
		string collisionObjectName = collision.gameObject.name;
		if (sourceObject != null && collisionObjectName.Equals (sourceObject.name))
		{
			return true;
		}

		if (collisionObjectName.Equals (gameObject.name))
		{
			return true;
		}

		return false;
	}

	public void setRange(float rangeToSet)
	{
		range = rangeToSet;
	}

	public void setDamage(float damageToSet)
	{
		damage = damageToSet;
	}

	public void setTargetTag(string target)
	{
		targetTag = target;
	}

	public void setSpeed(float speedToSet)
	{
		speed = speedToSet;
	}

	public void setSource(GameObject source)
	{
		sourceObject = source;
	}
}
