﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IShoot 
{
	void fire(Vector3 fireDirection);
}
