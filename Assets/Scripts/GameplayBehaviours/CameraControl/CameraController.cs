﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	GameplayManager manager;
    CameraShake camShake;

	void Start()
	{
		manager = GameObject.Find ("GameplayManager").GetComponent<GameplayManager> ();
        camShake = GetComponent<CameraShake>();
	}

    private void LateUpdate()
    {
		Vector3 averagePosition = getAveragePositionVectorOfAllPlayers(manager.getAllPlayers());
        Vector3 finalPosition = camShake.runShakeFrame(averagePosition);

        transform.position = finalPosition;
    }

	private Vector3 getAveragePositionVectorOfAllPlayers(List<GameObject> allPlayers)
	{
		Vector3 averagePosition = Vector3.zero;

		foreach(GameObject player in allPlayers)
		{
            if(player.activeSelf)
            {
                averagePosition += player.transform.position;
            }
		}
		averagePosition.x = averagePosition.x / allPlayers.Count;
		averagePosition.y = transform.position.y;
		averagePosition.z = averagePosition.z / allPlayers.Count;

		return averagePosition;
	}
}
