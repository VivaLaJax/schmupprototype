﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
Simple script that sends the collided player object to the spawn point when they hit
the end point.
*/
public class EndPointCollision : MonoBehaviour 
{
	GameObject spawnPoint;

	void Start () 
	{
		//Could take this from one stored in player, to do checkpoints
		spawnPoint = GameObject.Find("SpawnPoint");
		if(spawnPoint==null)
		{
			Debug.LogError("Oh no! Couldn't fidn the spawn point!");
		}
	}

	void OnCollisionEnter(Collision collisionInfo)
	{
		collisionInfo.gameObject.transform.position = spawnPoint.transform.position;
	}
}
