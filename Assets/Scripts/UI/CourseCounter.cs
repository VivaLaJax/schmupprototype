﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CourseCounter : MonoBehaviour 
{
	public GameObject playerToWatch;
	public GameObject course;
	public GameObject[] endPositions;
	float startDistance;

	float startCoursePos;
	float endCoursePos;
	float courseWidth;

	RectTransform rectTransform; 

	void Start () 
	{
		rectTransform = gameObject.GetComponent<RectTransform> ();
		if(playerToWatch==null) 
		{
			Debug.LogError ("Uh oh! Player to watch is NULL. This isn't gonna work!");
		}
		startCoursePos = rectTransform.anchoredPosition.x;
		endCoursePos = startCoursePos + course.GetComponent<RectTransform>().sizeDelta.x;
		courseWidth = endCoursePos - startCoursePos;

		startDistance = getCurrentDistanceFromClosestEnd ();
	}

	void Update () 
	{
		//Find closest end position
		float currentDistance = getCurrentDistanceFromClosestEnd ();

		//divide by start position should give you a percentage decimal
		if(currentDistance>startDistance)
		{
			currentDistance = startDistance;
		}
		float distaceToTravel = startDistance - currentDistance;
		float distancePercentageDecimal = distaceToTravel/startDistance;
		//position is then the startPos + courseWidth*percentageDecimal
		rectTransform.anchoredPosition = new Vector2(startCoursePos+(courseWidth*distancePercentageDecimal),
													rectTransform.anchoredPosition.y);
	}

	float getCurrentDistanceFromClosestEnd()
	{
		float closestEndpoint = 100000f;

		foreach(GameObject endpoint in endPositions)
		{
			float distance = Vector3.Distance (playerToWatch.transform.position, endpoint.transform.position);
			if(distance<closestEndpoint)
			{
				closestEndpoint = distance;
			}
		}

		return closestEndpoint;
	}
}
