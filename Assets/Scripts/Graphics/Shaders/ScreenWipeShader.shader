﻿Shader "Custom/ScreenWipeShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_TransitionTex("Transition Texture", 2D) = "white" {}
		_Cutoff("Cutoff", Range(0,1))=0
		_Color("Color", Color) = (1,1,1,1)
	}
	
	SubShader
	{
		/*Tags
		{
		"Queue" = "Transparent"
		}*/
		// No culling or depth
		Cull Off ZWrite Off ZTest Always
		//Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uv1 : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				o.uv1 = v.uv;

				#if UNITY_UV_START_AT_TOP
				if (_MainTex_TexelSize.y < 0)
				{
					o.uv1.y = 1 - o.uv1.y
				}
				#endif
				return o;
			}

			sampler2D _MainTex;
			sampler2D _TransitionTex;
			float _Cutoff;
			fixed4 _Color;

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 transition = tex2D(_TransitionTex, i.uv);

				if (transition.b < _Cutoff)
				{
					return _Color;
				}

				return tex2D(_MainTex, i.uv);
			}
			ENDCG
		}
	}
}
