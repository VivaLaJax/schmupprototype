﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Find closest thing in range, call interact on it and pass reference of player.
 */
public class InitiateInteraction : MonoBehaviour
{
    private InteractableWatcher interactableWatcher;

	void Start ()
    {
        interactableWatcher = gameObject.GetComponent<InteractableWatcher>();
	}

    public void attemptInteraction()
    {
        Debug.Log("Attempting to find interactable.");
        List<Interactable> nearestInteractableScripts = interactableWatcher.findClosestInteractable();
        if(nearestInteractableScripts != null)
        {
            Debug.Log("Interactable found! Attempting to interact.");
            foreach (Interactable nearestInteractable in nearestInteractableScripts)
            {
                nearestInteractable.interact(gameObject.GetComponent<Character>());
            }
        }
        else
        {
            Debug.Log("<color=#550000>Interactable not found =(</color>");
        }
    }
}
