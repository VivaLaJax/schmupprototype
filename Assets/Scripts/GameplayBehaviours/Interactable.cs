﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This interface acts as the idenfitfier for the interaction system.
 * It is passed a character class which is intended to be either a player
 * or NPC to allow us to have complex interactions from NPCs if we want to
 * set it up that way. 
 */
public class Interactable : MonoBehaviour
{
    float radius = 50.0f;
    List<GameObject> interactorsInRange;

    public void Start()
    {
        interactorsInRange = new List<GameObject>();
    }

    private void Update()
    {
        //We could replace these with Unity's physics collision detection instead
        //I don't know if that's more efficient or not
        checkOldInteractorsInRadius();
        checkNewInteractorsInRadius();
    }

    public virtual void interact(Character character)
    {
        //This needs to be overridden in implementations
    }

    void checkOldInteractorsInRadius()
    {
        if(interactorsInRange.Count==0)
        {
            return;
        }
        //Check the list of interactors you're registered with
        //and unregister from the ones that are outside the radius
        for(int i=interactorsInRange.Count-1; i>=0; --i)
        {
            GameObject interactor = interactorsInRange[i];
            if(Vector3.Distance(gameObject.transform.position,interactor.transform.position)>radius)
            {
                InteractableWatcher watcher = interactor.GetComponent<InteractableWatcher>();
                watcher.unregisterInteractable(gameObject);
                interactorsInRange.Remove(interactor);
            }
        }
    }

    void checkNewInteractorsInRadius()
    {
        //Find all objects in our radius
        //Find out if they have an interactable watcher and register self
        Vector3 center = transform.position;
        Collider[] hitColliders = Physics.OverlapSphere(center, radius);
        foreach(Collider hitCollider in hitColliders)
        {
            InteractableWatcher watcher = hitCollider.gameObject.GetComponent<InteractableWatcher>();
            if(watcher!=null && !interactorsInRange.Contains(hitCollider.gameObject))
            {
                watcher.registerInteractable(gameObject);
                interactorsInRange.Add(hitCollider.gameObject);
            }
        }
    }
}
