﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Command
{
    public abstract void execute(Actor actor);
    /*Can also have undo here*/
}
