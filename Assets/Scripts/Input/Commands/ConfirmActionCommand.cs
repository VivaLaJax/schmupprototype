﻿using System;

public class ConfirmActionCommand : Command
{
    public override void execute(Actor actor)
    {
        actor.onConfirm();
    }
}