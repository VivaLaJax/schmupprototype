﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoConversation : MonoBehaviour
{
    List<DialogueNode> conversation;

    void Start()
    {
        conversation = new List<DialogueNode>();

        DialogueNode nodeOne = new DialogueNode();
        nodeOne.Speaker = "Mrs. Cubes";
        nodeOne.NodeName = "Begin";
        nodeOne.DialogueText = "Why hello there, my dear wee lamb. I'm...not exactly sure where we are right now. Some sort of city I guess...";
        nodeOne.NextNodes = new List<string>() { "Question" };
        nodeOne.OptionDialogueText = new List<string>();
        conversation.Add(nodeOne);

        DialogueNode nodeTwo = new DialogueNode();
        nodeTwo.Speaker = "Mrs. Cubes";
        nodeTwo.NodeName = "Question";
        nodeTwo.DialogueText = "What kind of place do you think this is?";
        nodeTwo.NextNodes = new List<string>() { "Optimist", "Fourth Wall" };
        nodeTwo.OptionDialogueText = new List<string>() { "An exciting and busy place, full of possibility!", "It's just a temporary level Cubes, get a grip."};
        conversation.Add(nodeTwo);

        DialogueNode nodeThree = new DialogueNode();
        nodeThree.Speaker = "Mrs. Cubes";
        nodeThree.NodeName = "Optimist";
        nodeThree.DialogueText = "Oh, I do hope so young...sphere person. Have a wonderful day!";
        nodeThree.NextNodes = new List<string>() { DialogueManager.END_NODE };
        nodeThree.OptionDialogueText = new List<string>();
        conversation.Add(nodeThree);

        DialogueNode nodeFour = new DialogueNode();
        nodeFour.Speaker = "Mrs. Cubes";
        nodeFour.NodeName = "Fourth Wall";
        nodeFour.DialogueText = "Well! I never!";
        nodeFour.NextNodes = new List<string>() { DialogueManager.END_NODE };
        nodeFour.OptionDialogueText = new List<string>();
        conversation.Add(nodeFour);
    }

    public List<DialogueNode> getConversation()
    {
        return conversation;
    }

    public void receiveConversationPath(List<DialogueNode> route)
    {

    }
}
