﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTimeUtility 
{
	private static readonly float NUMBER_SECONDS_PER_DAY = 30.0f;

	public static string getTimeStringInGameTimeUnits(float timeToConvert)
	{
		return getTimeInGameDays(timeToConvert) + getGameUnits();
	}

	private static int getTimeInGameDays(float timeToConvert)
	{
		return (int)(timeToConvert/NUMBER_SECONDS_PER_DAY);
	}

	private static string getGameUnits()
	{
		return " days";
	}
}
