﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
Need to clean this up, the movement is not very precise at all which will
be frustrating.
*/
public class PlayerMovement : MonoBehaviour 
{
	PlayerCharacter playerCore;

	void Start()
	{
		playerCore = gameObject.GetComponent<PlayerCharacter> ();
	}

	public void onHorizontalInput(float horizontalInputValue)
    {
        if (horizontalInputValue>0)
        {
			transform.Translate(transform.right * Time.deltaTime * playerCore.getSpeed());
        }
        else if (horizontalInputValue<0)
        {
			transform.Translate(transform.right * Time.deltaTime * playerCore.getSpeed() * -1);
        }
    }

    public void onVerticalInput(float verticalInputValue)
    {
        if (verticalInputValue>0)
        {
			transform.Translate(transform.forward * Time.deltaTime * playerCore.getSpeed());
        }
        else if(verticalInputValue<0)
        {
			transform.Translate(transform.forward * Time.deltaTime * playerCore.getSpeed() * -1);
        }
    }

	public void onFullAxisInput(float horizontalInputValue, float verticalInputValue)
	{
        if (!gameObject.activeSelf)
        {
            return;
        }

        onHorizontalInput (horizontalInputValue);
		onVerticalInput (verticalInputValue);
	}
}
