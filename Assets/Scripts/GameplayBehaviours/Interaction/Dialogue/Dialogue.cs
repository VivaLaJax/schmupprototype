﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dialogue : MonoBehaviour, Actor
{
    DialogueNode currentNode;
    public DialogueManager dialogueManager;
    int currentSelectedOption;

    void Update()
    {
        //We could add animation for the dialogue text in here.
    }

    public void setDialogueManager(DialogueManager manager)
    {
        dialogueManager = manager;
    }

    public void setNode(DialogueNode node)
    {
        currentNode = node;
        currentSelectedOption = 0;
        setNameText(node.Speaker);
        setDialogText(node.DialogueText);
        if(node.NextNodes.Count>1)
        {
            setOptions(node.OptionDialogueText);
        }
    }

    private void setDialogText(string dialogueText)
    {
        Transform dialogue = transform.FindChild("DialoguePanel/DialogueText");
        dialogue.GetComponent<Text>().text = dialogueText;
    }

    private void setNameText(string speaker)
    {
        Transform nameText = transform.FindChild("NamePanel/NameText");
        nameText.GetComponent<Text>().text = speaker;
    }

    private void setOptions(List<string> optionDialogueText)
    {
        //At the moment, we're assuming the dialogue UI has beenc created with the
        //right number of option placeholders for us to change
        for(int i=0; i<optionDialogueText.Count; ++i)
        {
            Transform optionText = transform.FindChild("DialoguePanel/OptionText_" + (i + 1));
            optionText.GetComponent<Text>().text = optionDialogueText[i];
        }

        changeSelectedOption(0);
    }

    public void changeSelectedOption(int optionIndex)
    {
        if(optionIndex!=currentSelectedOption)
        {
            Transform prevOptionText = transform.FindChild("DialoguePanel/OptionText_" + (currentSelectedOption+ 1));
            prevOptionText.GetComponent<Text>().color = Color.black;
        }

        currentSelectedOption = optionIndex;
        //Highlight text
        Transform optionText = transform.FindChild("DialoguePanel/OptionText_" + (optionIndex + 1));
        optionText.GetComponent<Text>().color = Color.white;
    }

    public void selectOption()
    {
        dialogueManager.moveToNextNode(currentNode.NextNodes[currentSelectedOption]);
    }

    public void onVerticalInput(float verticalAxisAmount)
    {
        if(verticalAxisAmount<0)
        {
            changeSelectedOption((currentSelectedOption + 1) % currentNode.OptionDialogueText.Count);
        }
        else
        {
            int nextOption = currentSelectedOption - 1;
            if(nextOption<0)
            {
                nextOption = currentNode.OptionDialogueText.Count - 1;
            }
            changeSelectedOption(nextOption);
        }
    }

    public void onConfirm()
    {
        selectOption();
    }

    public void onCancel(){}
    public void onMenu(){}
    public void onDebugInput(){}
    public void onHorizontalInput(float horizontalAxisInput){}
	public void onSecondaryAxisHorizontalInput (float horizontalAxisInput){}
	public void onSecondaryAxisVerticalInput (float verticalAxisAmount){}
	public void onFullPrimaryAxisInput (float horizontalAxisInput, float verticalAxisInput){}
	public void onFullSecondaryAxisInput (float horizontalAxisInput, float verticalAxisInput){}
}