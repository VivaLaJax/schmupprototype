# Schmup Prototype #

This was a little multiplayer prototype I was making in my spare time, while my day job and basketball hobby were particularly heavy on my time. I had intended to keep the scope very small to get it to be a more attainable goal. The game is a coop schmup with some little pieces like screen shake implemented. It was intended to be class based, with some rpg elements thrown in for good measure.

Heavily inspired by the Geometry Wars games, the gameplay is pretty basic move around and dodge enemies.

Importantly in this game I develoepd the first iteration of an Input Manager system which uses a command queue and then fires the commands as events. This is generic enough that I have used the system in other games since and iterated on it to handle button holds and releases as different events. I'm pretty happy with the system-style work I did there, as it's easy to extend and can be used for much more complex ganes in the future.

Here's how it turned out (gif recorded at 15fps to save space!):

![Alt Text](https://media.giphy.com/media/n3OJjeldJXdmmrQ2PH/giphy.gif)