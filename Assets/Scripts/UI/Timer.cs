﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour 
{
	GameplayManager gameplayManager;
	Text timerText;
	
	void Start () 
	{
		gameplayManager = GameObject.Find("GameplayManager").GetComponent<GameplayManager>();
		timerText = gameObject.GetComponent<Text>();
	}
	
	void Update () 
	{
		string gameTime = GameTimeUtility.getTimeStringInGameTimeUnits(gameplayManager.getCurrentGameTime());
		if(timerText.text!=gameTime)
		{
			timerText.text = gameTime;
		}
	}
}
