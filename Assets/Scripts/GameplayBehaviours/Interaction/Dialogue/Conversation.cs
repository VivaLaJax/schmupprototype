﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conversation : Interactable
{
    public GameObject dialogueManager;
    public TextAsset conversationFile;
    DialogueNodeCollection conversation;

    new void Start()
    {
        base.Start();
        conversation = new DialogueNodeCollection();

        if (conversationFile!=null)
        {
            conversation = JsonUtility.FromJson<DialogueNodeCollection>(conversationFile.text);
            Debug.Log("The read in conversation: " + conversationFile.text);
            Debug.Log("Conversation object: " + conversation.ToString());
        }
        
    }

    public List<DialogueNode> getConversation()
    {
        return conversation.getNodes();
    }

    public void receiveConversationPath(List<DialogueNode> route)
    {
        //This should probably pass on to some kind of interface
    }

    public override void interact(Character character)
    {
        dialogueManager.GetComponent<DialogueManager>().beginDialogue(getConversation(), gameObject);
    }
}
