﻿using System;

public interface Actor
{
	void onHorizontalInput(float horizontalAxisInput);
    void onVerticalInput(float verticalAxisAmount);
    void onCancel();
    void onMenu();
    void onDebugInput();
    void onConfirm();
	void onSecondaryAxisHorizontalInput(float horizontalAxisInput);
	void onSecondaryAxisVerticalInput(float verticalAxisAmount);
	void onFullPrimaryAxisInput(float horizontalAxisInput, float verticalAxisInput);
	void onFullSecondaryAxisInput(float horizontalAxisInput, float verticalAxisInput);
}