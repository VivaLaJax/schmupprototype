﻿using System;

public class MenuActionCommand : Command
{
    public override void execute(Actor actor)
    {
        actor.onMenu();
    }
}