﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour, Damageable
{
	public GameObject gameplayManagerObject;
	GameplayManager gameplayManager;
    CameraShake camShake;

    private AudioSource source;
    public AudioClip deathSound;

	public int playerIndex;

	public int health;
	public int maxHealth = 200;

	public float speed = 30.0f;

	public int lives;
	public int maxLives = 3;

    private Vector3 spawnPosition;

	void Start()
	{
        source = GetComponent<AudioSource>();
		gameplayManager = gameplayManagerObject.GetComponent<GameplayManager> ();
        camShake = GameObject.Find("Camera").GetComponent<CameraShake>();
        spawnPosition = transform.position;
		reset ();
	}

	void Update()
	{
		checkHealth ();
	}

	public void reset()
	{
		lives = maxLives;
		health = maxHealth;
        resetPosition();
	}

    private void resetPosition()
    {
        transform.position = spawnPosition;
    }

    public int getPlayerIndex()
	{
		return playerIndex;
	}

	public int getCurrentHealth()
	{
		return health;
	}

	public int getMaxHealth()
	{
		return maxHealth;
	}

	private void resetHealth()
	{
		health = maxHealth;
	}

	public float getHealthPercentageDecimal()
	{
		return ((float)health / (float)maxHealth);
	}

	public float getSpeed()
	{
		return speed;
	}

	public int getLives()
	{
		return lives;
	}

	public bool isAlive()
	{
		return lives != 0;
	}

	public void onDamageReceived (float damageValue)
	{
		health -= (int)damageValue;
        camShake.shake();
	}

	private void checkHealth()
	{
		if (health <= 0) 
		{
			playerDied ();
		}
	}

	private void playerDied()
	{
        AudioSource.PlayClipAtPoint(deathSound,transform.position);
		lives = lives - 1;
		gameplayManager.onPlayerDied (gameObject);
		if (lives > 0)
		{
			resetHealth ();
            resetPosition();
		} 
		else
		{
			gameObject.SetActive (false);
		}
	}

    public void playSoundEffect(AudioClip clip)
    {
        source.PlayOneShot(clip);
    }
}
