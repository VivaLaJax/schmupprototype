﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Need to find out if you've hooked up the interactable stuff right,
 * but don't want to write new interactable code? Have this one print
 * out some debug messages on interact.
 * 
 */
public class DebugCheckInteract : Interactable
{
    public override void interact(Character character)
    {
        Debug.LogFormat("<color=#008000ff>I am interacting!</color>");
    }
}
