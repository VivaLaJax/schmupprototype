﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public float shakeDuration = 5.0f;
    public float shakeOffset = 0.6f;
    public bool isComposite = true;

    private Transform camTransform;
    private Vector3 originalPosition;
    public bool shaking;
    private float shakeTimer;

    void Awake()
    {
        camTransform = transform;
        shaking = false;
    }

    void LateUpdate()
    {
        runShakeFrame(camTransform.position);
    }

    public void shake()
    {
        originalPosition = camTransform.position;
        shaking = true;
        shakeTimer = 0;
    }

    public Vector3 runShakeFrame(Vector3 positionToShake)
    {
        if (!shaking)
        {
            return positionToShake;
        }

        updateTimer();
        return shakePosition(positionToShake);
    }

    private Vector3 shakePosition(Vector3 positionToShake)
    {
        Vector3 shakePos = positionToShake + (UnityEngine.Random.insideUnitSphere * shakeOffset);

        if (!isComposite)
        {
            camTransform.position = shakePos;
        }

        return shakePos;
    }

    private void updateTimer()
    {
        shakeTimer += Time.deltaTime;
        if (shakeTimer > shakeDuration)
        {
            shaking = false;
        }
    }
}
