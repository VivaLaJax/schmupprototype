﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour, Damageable 
{
	private const int PROBABILITY_FIRST = 50;
	private const int PROBABILITY_SECOND = 75;

	public float health = 5.0f;
	public float speed = 20.0f;
	public float damage = 50.0f;

	public List<GameObject> allPlayers;

    private int pointsValue = 20;
    public int level = 1;

    private AudioSource source;
    public AudioClip deathSound;
    private CameraShake camShake;

    void Start () 
	{
        source = GetComponent<AudioSource>();
        camShake = GameObject.Find("Camera").GetComponent<CameraShake>();
        GameplayManager manager = GameObject.Find("GameplayManager").GetComponent<GameplayManager>();
		allPlayers = manager.getAllActivePlayers ();
		manager.onEnemySpawned (gameObject);
	}
	
	void Update () 
	{
		checkHealth ();
		moveSelf ();
	}

	void OnDestroy()
	{
        AudioSource.PlayClipAtPoint(deathSound, transform.position);
        GameplayManager manager = GameObject.Find("GameplayManager").GetComponent<GameplayManager>();
		manager.onEnemyDestroyed (gameObject, getPoints());
	}

	public void onDamageReceived (float damageValue)
	{
		health -= damageValue;
        camShake.shake();
        Debug.Log ("Damage has been received. Health is now: " + health);
	}

	public float getDamage()
	{
		return damage;
	}

	private void checkHealth()
	{
		if (health <= 0) 
		{
			Destroy (gameObject);
		}
	}

    public void setLevel(int level)
    {
        this.level = level;
    }

    public int getPoints()
    {
        return pointsValue;
    }

	private void moveSelf()
	{
		GameObject closestPlayer = findClosestPlayer();
		Vector2 directionFromSelf = getDirectionRepresentationOfClosestPlayerFromSelf(closestPlayer);
		moveFavouringPlayerDirection(directionFromSelf);
	}

	private GameObject findClosestPlayer()
	{
		GameObject closestPlayer = null;
		float closestDistance = 9999f;
		Vector3 position = gameObject.transform.position;
		foreach(GameObject player in allPlayers)
		{
			float distance = Vector3.Distance (position, player.transform.position);
			if (distance < closestDistance) 
			{
				closestPlayer = player;
				closestDistance = distance;
			}
		}

		return closestPlayer;
	}

	private Vector2 getDirectionRepresentationOfClosestPlayerFromSelf(GameObject closestPlayer)
	{
		Vector2 directionRepresentation;
		Vector3 position = gameObject.transform.position;

		if (closestPlayer.transform.position.x < position.x) 
		{
			directionRepresentation.x = -1;
		}
		else 
		{
			directionRepresentation.x = 1;
		}

		if (closestPlayer.transform.position.z < position.z) 
		{
			directionRepresentation.y = -1;
		}
		else 
		{
			directionRepresentation.y = 1;
		}

		return directionRepresentation;
	}

	private void moveFavouringPlayerDirection(Vector2 directionRepresentation)
	{
		Vector3 direction = new Vector3 (0, 0, 0);
		Vector3 probabilityVector = getProbabilityVector ((int)directionRepresentation.x);
		direction.x = getSeededRandomValueFromVector(probabilityVector);

		probabilityVector = getProbabilityVector ((int)directionRepresentation.y);
		direction.z = getSeededRandomValueFromVector(probabilityVector);

		transform.position += direction * Time.deltaTime * (speed*level);
	}

	private Vector3 getProbabilityVector(int value)
	{
		Vector3 probabilityVector = new Vector3 (0,0,0);
		if (value == 0)
		{
			probabilityVector.x = value;
			probabilityVector.y = value - 1;
			probabilityVector.z = value + 1;
			return probabilityVector;
		}

		if (value == 1)
		{
			probabilityVector.x = value;
			probabilityVector.y = value - 1;
			probabilityVector.z = value - 2;
			return probabilityVector;
		}

		if (value == -1)
		{
			probabilityVector.x = value;
			probabilityVector.y = value + 1;
			probabilityVector.z = value + 2;
			return probabilityVector;
		}

		return probabilityVector;
	}

	private int getSeededRandomValueFromVector(Vector3 probabilityVector)
	{
		int probability = UnityEngine.Random.Range (0, 100);
		if (probability < PROBABILITY_FIRST)
		{
			return (int)probabilityVector.x;
		} 
		else if (probability < PROBABILITY_SECOND)
		{
			return (int)probabilityVector.y;
		} 

		return (int)probabilityVector.z;
	}
}
