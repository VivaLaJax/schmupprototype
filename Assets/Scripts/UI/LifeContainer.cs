﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeContainer : MonoBehaviour 
{
	public GameObject playerToWatch;
	PlayerCharacter playerCharacter;

	public string playerExtension = "";

	int previousLifeCount;

	void Start () 
	{
		if(playerToWatch==null) 
		{
			Debug.LogError ("Uh oh! Player to watch is NULL. This isn't gonna work!");
		}
		playerCharacter = playerToWatch.GetComponent<PlayerCharacter> ();
		if(playerCharacter==null) 
		{
			Debug.LogError ("Uh oh! PlayerCharacter script is NULL. This isn't gonna work!");
		}
		showLives (playerCharacter.getLives());
		previousLifeCount = playerCharacter.getLives ();
	}

	void Update () 
	{			
		if (playerCharacter.getLives () != previousLifeCount)
		{
			showLives (playerCharacter.getLives ());
			previousLifeCount = playerCharacter.getLives ();
		}
	}

	private void showLives(int numberLives)
	{
		Image[] lifeIcons = gameObject.GetComponentsInChildren<Image> ();
		for(int i=0; i<lifeIcons.Length; ++i)
		{
			if (i < numberLives)
			{
				lifeIcons [i].enabled = true;
				continue;
			}
			lifeIcons [i].enabled = false;
		}
	}
}
