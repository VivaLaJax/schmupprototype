﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameoverScreen : MonoBehaviour, Actor
{
	GameplayManager manager;
	GameObject[] inputManagers;
    public GameObject score;

	void Start () 
	{
		manager = GameObject.Find("GameplayManager").GetComponent<GameplayManager>();
		inputManagers = GameObject.FindGameObjectsWithTag ("InputManager");
		takeInputFocus ();
	}

	void Update () 
	{
		
	}

    public void open()
    {
        score.GetComponent<Text>().text += manager.getPoints();
    }

	void takeInputFocus()
	{
		foreach (GameObject inputManagerObject in inputManagers)
		{
			InputManager inputManager = inputManagerObject.GetComponent<InputManager> ();
			inputManager.takeInputFocus (this);
		}
	}

	void releaseInputFocus()
	{
		foreach (GameObject inputManagerObject in inputManagers)
		{
			InputManager inputManager = inputManagerObject.GetComponent<InputManager> ();
			inputManager.relinquishInputFocus(this);
		}
	}

	public void onConfirm ()
	{
		manager.resetGame ();
		releaseInputFocus ();
	}

	public void onHorizontalInput (float horizontalAxisInput){}
	public void onVerticalInput (float verticalAxisAmount){}
	public void onCancel (){}
	public void onMenu (){}
	public void onDebugInput (){}
	public void onSecondaryAxisHorizontalInput (float horizontalAxisInput){}
	public void onSecondaryAxisVerticalInput (float verticalAxisAmount){}
	public void onFullPrimaryAxisInput (float horizontalAxisInput, float verticalAxisInput){}
	public void onFullSecondaryAxisInput (float horizontalAxisInput, float verticalAxisInput){}
}
