﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour 
{
	public GameObject objectToSpawn;

	float timeToNextSpawn;
	const float MIN_TIME_TO_SPAWN = 5f;
	const float MAX_TIME_TO_SPAWN = 10f;
	const int MAX_ENEMIES = 20;

	float lastSpawnTime;

	GameplayManager manager;

	void Start () 
	{
		manager = GameObject.Find ("GameplayManager").GetComponent<GameplayManager> ();
		resetTimeToSpawn ();
	}

	void Update () 
	{
		if (isTimeToSpawn () && !isLevelAtCapacity())
		{
			spawnObject ();
			resetTimeToSpawn ();
		}
	}

	public void reset()
	{
		resetTimeToSpawn ();
	}

	private bool isTimeToSpawn()
	{
		float timeSinceSpawn = Time.time - lastSpawnTime;
		return timeSinceSpawn >= timeToNextSpawn;
	}

	private bool isLevelAtCapacity()
	{
		return (manager.getEnemyCount () >= MAX_ENEMIES);
	}

	private void spawnObject()
	{
		GameObject spawnedEnemy = Instantiate (objectToSpawn, gameObject.transform.position, objectToSpawn.transform.rotation);
        spawnedEnemy.GetComponent<Enemy>().setLevel(manager.getLevel());
	}

	private void resetTimeToSpawn()
	{
		lastSpawnTime = Time.time;
		timeToNextSpawn = Random.Range (MIN_TIME_TO_SPAWN, MAX_TIME_TO_SPAWN);
	}
}
