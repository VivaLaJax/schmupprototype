﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/* The aim of this class is to run on anything that wishes to keep track
 * of interactables that they are close enough to consider for interacting
 * with.
 * 
 * If we make it so the interactables have to look at objects that come into
 * their range and tell them that they can be interacted with, then characters
 * don't have to work through every object that's close to them and check to see
 * if it's an interactable. They can just work through a list which is being maintained.
 * This keeps the code for interacting a little tidier.
 * 
 * Could also organise that list by distance if needed.
 */
class InteractableWatcher : MonoBehaviour
{
    List<GameObject> registeredInteractables;
    public float fieldOfVisionAngle = 120.0f;

    void Start()
    {
        registeredInteractables = new List<GameObject>();
    }

    void Update()
    {

    }

    public void registerInteractable(GameObject newInteractable)
    {
        Debug.Log(gameObject.name+" has registered the interactable: "+newInteractable.name);
        if (!registeredInteractables.Contains(newInteractable))
        {
            registeredInteractables.Add(newInteractable);
        }
    }

    public void unregisterInteractable(GameObject removedInteractable)
    {
        Debug.Log(gameObject.name + " has unregistered the interactable: " + removedInteractable.name);
        registeredInteractables.Remove(removedInteractable);
    }

    public List<Interactable> findClosestInteractable()
    {
        float closestInList = 9000.0f;
        GameObject closestInteractable = null;
        foreach(GameObject i in registeredInteractables)
        {
            //Need to disregard objects behind the character
            Vector3 directionVector = gameObject.transform.position - i.transform.position;
            float angle = Vector3.Angle(gameObject.transform.forward, directionVector);
            if(angle>(fieldOfVisionAngle/2))
            {
                Debug.Log("Skipping over " + i.name + " because they are out of vision (" + angle + "|" + fieldOfVisionAngle / 2 + ")");
                continue;
            }
            float dist = Vector3.Distance(gameObject.transform.position, i.transform.position);
            if (dist < closestInList)
            {
                closestInList = dist;
                closestInteractable = i;
            }
        }

        if(closestInteractable==null)
        {
            return null;
        }
        Debug.Log("Closest Interactable was: " + closestInteractable.name + " @ " + closestInList);

        return new List<Interactable>(closestInteractable.GetComponents<Interactable>());
    }
}