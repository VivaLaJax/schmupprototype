﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollision : MonoBehaviour 
{
	float damage;

	void Start () 
	{
		damage = gameObject.GetComponent<Enemy> ().getDamage ();
	}
	
	void Update () 
	{
		
	}

	void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.CompareTag ("Player"))
		{
			Damageable damageableInterface = collision.gameObject.GetComponent<Damageable> ();
			if (damageableInterface!=null) 
			{
				Debug.Log ("Dealing damage to the target");
				damageableInterface.onDamageReceived (damage);
			}
			Destroy (gameObject);
		}
	}
}
